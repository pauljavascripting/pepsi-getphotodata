import React, { Component } from 'react';
import {TweenMax,Linear} from 'gsap';
import cherry_image from '../images/cherry.png'
import lime_image from '../images/lime.png'
import vanilla_image from '../images/vanilla.png'
import instagram_image from '../images/instagram.png'

export class LoadAnimationSquare extends Component {

	componentDidMount(){

      const points1 = [{x:20, y:-60},{x:490, y:-60}, {x:490, y:500},{x:20, y:500}]
      const points2 = [{x:490, y:500},{x:20, y:500}, {x:20, y:-50},{x:490, y:-50}]
      const points3 = [{x:20, y:500}, {x:20, y:-50},{x:490, y:-50},{x:490, y:500}]
      const points4 = [{x:20, y:-50},{x:490, y:-60},{x:490, y:500},{x:20, y:500}]

      // cherry
      TweenMax.set(this.cherry, {x:20, y:500, scale:0.5})
      TweenMax.to(this.cherry, 12, {bezier:{curviness:0, values:points1}, ease:Linear.easeNone, paused:false, repeat:-1});

      // lime
      TweenMax.set(this.lime, {x:490, y:-50, scale:0.5})
      TweenMax.to(this.lime, 12, {bezier:{curviness:0, values:points2}, ease:Linear.easeNone, paused:false, repeat:-1});

      // vanilla
      TweenMax.set(this.vanilla, {x:490, y:500, scale:0.5})
      TweenMax.to(this.vanilla, 12, {bezier:{curviness:0, values:points3}, ease:Linear.easeNone, paused:false, repeat:-1});

      // instagram
      TweenMax.set(this.instagram, points4[1])
      //TweenMax.to(this.instagram, 12, {bezier:{curviness:0, values:points4}, ease:Linear.easeNone, paused:false, repeat:-1});
     

	}

	render(){

		return(

			<div>

        <div style={styles.instagram} ref={instagram => this.instagram = instagram} className="instagram" />
				<div style={styles.cherry} ref={cherry => this.cherry = cherry} className="cherry" />
				<div style={styles.lime} ref={lime => this.lime = lime} className="lime" />
				<div style={styles.vanilla} ref={vanilla => this.vanilla = vanilla} className="vanilla" />
        

			</div>

		)

	}

}

const styles = {
  vanilla: {
  	width: 128,
  	height: 108,
  	backgroundImage: `url(${vanilla_image})`,
  	marginLeft:-10,
  	marginTop:-110
  }, 
  cherry: {
  	width: 88,
  	height: 132,
  	backgroundImage: `url(${cherry_image})`,
    marginTop:-105

  },
   lime: {
  	width: 106,
  	height: 112,
  	backgroundImage: `url(${lime_image})`,
  	marginLeft:0,
  	marginTop:-120
  },
   instagram: {
    width: 64,
    height: 64,
    backgroundImage: `url(${instagram_image})`,
    marginTop:40
  },

}

export default LoadAnimationSquare