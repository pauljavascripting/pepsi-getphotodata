import React, { Component } from 'react';
import {TweenMax,Linear} from 'gsap';
import cherry_image from '../images/cherry.png'
import lime_image from '../images/lime.png'
import vanilla_image from '../images/vanilla.png'

export class LoadAnimationCircular extends Component {

	componentDidMount(){

	  TweenMax.set([this.cherry], {x:70, y:0, scale:0.5})
    TweenMax.to([this.cherry], 8, {bezier:[{ x:260, y:-60 }, {x:470, y:90}, {x:450, y:330}, {x:240, y:440}, {x:20, y:330}, {x:70, y:0}], ease:Linear.easeNone, paused:false, repeat:-1})
		
		TweenMax.set([this.lime], {x:470, y:90, scale:0.5})
		TweenMax.to([this.lime], 8, {bezier:[{x:440, y:310}, {x:240, y:420}, {x:20, y:310}, {x:50, y:-10}, { x:280, y:-70 }, {x:470, y:90}], ease:Linear.easeNone, paused:false, repeat:-1})		
  
		TweenMax.set([this.vanilla], {x:240, y:410, scale:0.5})
		TweenMax.to([this.vanilla], 8, {bezier:[{x:20, y:300}, {x:50, y:-20}, { x:320, y:-70 }, {x:470, y:90},{x:440, y:300}, {x:240, y:410}], ease:Linear.easeNone, paused:false, repeat:-1})		

	}

	render(){

		return(

			<div>

				<div style={styles.cherry} ref={cherry => this.cherry = cherry} className="cherry" />
				<div style={styles.lime} ref={lime => this.lime = lime} className="lime" />
				<div style={styles.vanilla} ref={vanilla => this.vanilla = vanilla} className="vanilla" />

			</div>

		)

	}

}

const styles = {
  vanilla: {
  	width: 128,
  	height: 108,
  	backgroundImage: `url(${vanilla_image})`,
  	marginLeft:-10,
  	marginTop:-110
  }, 
  cherry: {
  	width: 88,
  	height: 132,
  	backgroundImage: `url(${cherry_image})`
  },
   lime: {
  	width: 106,
  	height: 112,
  	backgroundImage: `url(${lime_image})`,
  	marginLeft:10,
  	marginTop:-90
  }

}

export default LoadAnimationCircular