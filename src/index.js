import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './containers/App';
//import serviceWorker from './serviceWorker';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import { InitialState } from './reducers/InitialState';

// state controller ---------------------------------------------------------------------------------- //
const mainReducer = (state = InitialState, action) => {
	
	switch(action.type){

		case 'mode_type':

			state = {

				...state,
				mode_type: action.data,

			}

			//console.log('mode_type:'+action.data)
			
		break;

		case 'trailer_update':

			state = {

				...state,
				trailer_complete: action.trailerComplete,
			}

		break;

		case 'face_detected':

			state = {

				...state,
				mode_type: action.modeType,
				user_name: action.name,
				response_message:action.message
			}

			//console.log('face_detected!!!!'+action.name+' '+action.distance+" "+action.message)

		break;

		case 'update_drinks':

			state = {

				...state,
				chosen_drinks: action.chosenDrinks,
			}


		break;

		case 'update_flavours':

			state = {

				...state,
				chosen_flavours: action.chosenFlavours,
			}


		break;

		case 'update_flavours_and_drinks':

			state = {

				...state,
				chosen_flavours: action.chosenFlavours,
				chosen_drinks: action.chosenDrinks,
			}
		
		//console.log(action.chosenFlavours+'....'+action.chosenDrinks)

		break;

		case 'response_drinks_intro':

			state = {

				...state,
				response_message: action.type,
				pour_drink:action.pourDrink,
				pouring:action.pouring
			}

			// console.log(action.pourDrink+' '+action.pouring)

		break;

		case 'response_drinks_menu':

			state = {

				...state,
				response_message: action.type

			}

		break;

		case 'response_drink_new':

			state = {

				...state,
				response_message: action.type,
				//chosen_drinks: [],
				//chosen_flavours: []
				//new_drink:action.newDrink,
			}

		break;

		case 'response_create_new_drink':

			state = {

				...state,
				response_message: action.type,
				chosen_drinks: [],
				chosen_flavours: []
				//new_drink:action.newDrink,
			}

		break;

		case 'response_drink_same':

			state = {

				...state,
				response_message: action.type,
				pour_drink:action.pourDrink
			}

		break;

		case 'response_drink_add':

			state = {

				...state,
				response_message: action.type,
				new_drink:action.newDrink

			}

		break;

		case 'response_drink_pouring':

			state = {

				...state,
				response_message: action.type,
				pouring:action.pouring
			}

		break;

		case 'set_nectar_points':

			state = {

				...state,
				nectar_points: action.nectarPoints

			}

			//console.log('Yes!: '+action.nectarPoints)
		break;

		case 'response_drinks_endscreen':

			state = {

				...state,
				response_message: action.type,
				end_screen:action.endScreen
			}

			// console.log(action.pourDrink+' '+action.pouring)

		break;

		case 'demo':

			state = {

				...state,
				mode_type: action.data,
				chosen_drinks: action.chosenDrinks,
				chosen_flavours: action.chosenFlavours,
				new_drink:action.newDrink,
				response_message:action.responseMessage,
				pour_drink:action.pourDrink,
				nectar_points:action.nectarPoints,
				pouring:action.pouring,
				end_screen:action.endScreen,
				trailer_complete:action.trailerComplete
			}
			//console.log(action.pouring)
		break;

		default:

		break;

	}

	return state;

}

// logger ---------------------------------------------------------------------------------- //
const myLogger = (store) => (next) => (action) => {

	//console.log('logged action: ', action)
	next(action); // pass action along to reducer
}

// instance of store ---------------------------------------------------------------------------------- //
export const store = createStore(mainReducer, {}, applyMiddleware(thunk, myLogger)); // Shorten params by using same name for parameter property & value in ES6

// set app values in order to start demo
store.dispatch({ type: 'demo', data:'passive', responseMessage:'welcome', chosenDrinks:[2], newDrink:2, chosenFlavours:[0,1], pourDrink:false, pouring:false, nectarPoints:0, endScreen:false, trailerComplete:false  })

//store.dispatch({ type: 'mode_type', data:'passive', chosenDrinks:[1,4,5], newDrink:2, chosenFlavours:[1,2] })

// render ---------------------------------------------------------------------------------- //
ReactDOM.render(

	<Provider store={store}> 
		<App />
	</Provider>,
	document.getElementById('root')

);

//serviceWorker();

